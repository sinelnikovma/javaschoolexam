package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class  PyramidBuilder {

	public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException,NullPointerException {
		int length = inputNumbers.size();
		int rowCount = 0;				// number of lines
		int discriminant = 0;
		double result = 0;				// result of square root of discriminant
		int currentPosition = 0;		//position in input List

		if(length == 0){
			throw new CannotBuildPyramidException();
		}

		for(Integer i : inputNumbers){
			if(i == null){
				throw new CannotBuildPyramidException();
			}
		}

		// we need n*(n-1)/2 = count of elements in input list for right building
		//for definition n should solve the equation n2 + n - 2*count = 0

		discriminant = 1 + 8 * length;		// D = b2 - 4ac
		result = Math.sqrt(discriminant);

		if(result % 1 != 0){
			throw new CannotBuildPyramidException();
		}

		rowCount = ((int)result - 1) / 2;	// (-b + sqrt(D)) / 2a
		Collections.sort(inputNumbers);


		int[][] resultArray = new int[rowCount][2 * rowCount - 1];

		for(int i = 0; i < rowCount; i++){
			int firstPosition = rowCount - i - 1;
			for(int j = 0; j < i + 1; j++){

				if(inputNumbers.get(currentPosition) == null){
					throw new CannotBuildPyramidException();
				}

				resultArray[i][firstPosition + j * 2] = inputNumbers.get(currentPosition++);
			}

		}


		return resultArray;
	}
}