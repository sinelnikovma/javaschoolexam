package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class  Subsequence{

	public boolean find(List target, List source){

		int lengthTarget;
		int lengthSource;
		int lastPosition = -1;

		try{
			lengthTarget = target.size();
			lengthSource = source.size();
		} catch(NullPointerException ex){
			throw new IllegalArgumentException();
		}

		if(lengthTarget == 0){
			return true;
		}

		if(lengthSource == 0 && lengthTarget > lengthSource){
			return false;
		}

		for(int i = 0; i < lengthTarget; i++){

			if(i == 0){
				lastPosition = source.indexOf(target.get(i));
				if(lastPosition == -1){
					return false;
				}
			}

			int currentPosition = source.indexOf(target.get(i));

			if(currentPosition == -1){
				return false;
			}

			if(lastPosition > currentPosition){
				return false;
			}
		}

		return true;
	}
}
